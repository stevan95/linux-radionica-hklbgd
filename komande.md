# Osnovne komande u terminalu

## Navigacija i rad sa folderima

- `pwd`, ispisuje putanju do foldera u kome smo sada

- `ls`, ispisuje sadrzaj trenutnog foldera
	* `ls -l`, ispisuje sadrzaj trenutnog foldera u obliku liste
	* `ls -al`, ispisuje sav sadrzaj, i "skrivene" fajlove koji pocinju sa tackom u obliku liste
- `cd <neki_folder>`, prelazimo iz foldera u folder
	* `cd ..`, prezlazimo u folder koji je iznad trenutnog
	* `cd`, prelazimo u `home` folder trenutnog korisnika
- `mkdir <ime_foldera>`, pravimo novi folder unutar trenutnog 
	* `mkdir <folder1> <folder2> <...>`, mozemo praviti vise foldera odjednom
- `cp <file1> <file2>`, kopira fajl1 u fajl2 
- `mv <file1> <folder>`, prebacuje fajl1 u folder koji zelimo
	* `mv <file1> <file2> <file3> <folder>`, sve fajlove prebacuje u folder koji je poslednji naveden
- `rm <file1> <file2>`, brise navedene fajlove
	* `rm -r <folder1> <folder2>`, brise foldere, `rm` bez `-r` brise samo fajllove

## Rad sa fajlovima
- `touch <ime_fajla>`, pravi novi fajl u trenutnom folderu
- `cat <file1>`, ispisuje sadrzaj fajla, korisno za tekstualne fajlove, od drugih formata moza da zabaguje, probajte
	* `cat <file1> <file2> <...>', ispisuje sadrzaj svih navedenih fajlova za redom
- `nano <fajl>`, editor tekstualnih fajlova u terminalu, jednostavan i koristan
- `less <file>`, ako je fajl predugacak less omogucava skrolovanje
- `head <file>`, ispisuje samo prvi deset linija falja
	* `head <file> -n <broj>`, ispisuje prvih n linija fajla
- `tail <file>`, isto kao `head` samo sto ispisuje poslednje linije umesto prvih
- 'tac <file>', ispisuje sadrzaj faja unazad, isto kao `cat` podrzava vise fajlova

## Odrzavanje paketa sa apt
- `sudo apt update`, update-uje bazu paketa, treba raditi pre `upgrade`-a
- `sudo apt upgrade`, instalira nove update-e
- `sudo apt install <paket1> <paket2> <..>`, instalira trazene pakete
- `apt search <kljucna_rec>`, ispisuje sve pakete koji odgovaraju pretrazi
- `sudo apt remove <paket1> <paket2> <...>`, brise navedene pakete
- `sudo apt purge <paket1> <paket2> <...>`, brise navedene pakete i njihove zavisnosti
- `sudo apt autoremove`, brise pakete koji nisu neophodni vise, obicno zavisnosti koje se vise ne koriste
- `apt show <paket>`, ispisuje detaljnije podatke o paketu


## Pomoc pri upotrebi komandi
- `man <komanda>`, ispisuje man stranicu o komandu, njeno uputstvo za upotrebu
- `man -k <kljucna_rec>`, pretrazuje man stranice po kljucnoj reci
- `info <komanda>`, slicno `man` komadni, malo je drugaciji interfejs
- `<komanda> -h`, obicno svaka komanda ima ugradjenu help opciju u sebi
	* `<komanda> --help`, nekada mora da se koristi i `--help` 


## Alternativni shell
- [fish](https://fishshell.com/), `friendly interactive shell`, bolji autocomplete, pruza dosta pomoci pri radu

## Tips & tricks
- pri kucanju koristite `<TAB>` to je autcomplete za shell
- shell pamti istoriju komandi
	* da bi ponovoli prethodnu komandu ukucajte strelicu na gore
	* `history`, ispisuje ceo sadrzaj
	* `history -c`, brise ceo sadrzaj
	* `history clear`, brise istorju ukoliko koristite `fish` shell
- zanimljivi [ascii art](https://en.wikipedia.org/wiki/ASCII_art) programi, svi se mogu instalirati sa `apt`
	* `cmatrix`, matrix screensaver
	* `figlet`, pravi asciiart od zadate reci
	* `cowsay`, krava koja govori
	* `fortune`, fortune cookie za terminal
