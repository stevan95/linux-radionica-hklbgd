## Alternativni shell
- [fish](https://fishshell.com/), `friendly interactive shell`, bolji autocomplete, pruza dosta pomoci pri radu

## Tips & tricks
- pri kucanju koristite `<TAB>` to je autcomplete za shell
- shell pamti istoriju komandi
	* da bi ponovoli prethodnu komandu ukucajte strelicu na gore
	* `history`, ispisuje ceo sadrzaj
	* `history -c`, brise ceo sadrzaj
	* `history clear`, brise istorju ukoliko koristite `fish` shell
- zanimljivi [ascii art](https://en.wikipedia.org/wiki/ASCII_art) programi, svi se mogu instalirati sa `apt`
	* `cmatrix`, matrix screensaver
	* `figlet`, pravi asciiart od zadate reci
	* `cowsay`, krava koja govori
	* `fortune`, fortune cookie za terminal
